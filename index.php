<?php

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Symfony\Component\DomCrawler\Crawler;

class BartEhrmanBlogScraper
{
    const ARTICLE_PATH = 'articles/%s.txt';

    /**
     * @var Client
     */
    private $config;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string[]
     */
    private $categoriesToScrape;

    /**
     * @var string[]
     */
    private $articlePageLinks;

    public function __construct()
    {
        $this->config = CookieJar::fromArray(['cookieName' => 'cookieValue',], 'ehrmanblog.org');
        $this->client = $client = new Client();

        $this->categoriesToScrape = [
            'https://ehrmanblog.org/new-testament-manuscripts/',
            'https://ehrmanblog.org/religion-in-the-news/',
            'https://ehrmanblog.org/greco-roman-religions-and-culture/',
            'https://ehrmanblog.org/hebrew-bible-old-testament/',
            'https://ehrmanblog.org/historical-jesus/',
            'https://ehrmanblog.org/canonical-gospels/',
            'https://ehrmanblog.org/history-of-christianity-100-300ce/',
            'https://ehrmanblog.org/catholic-epistles/',
            'https://ehrmanblog.org/early-christian-apocrypha/',
            'https://ehrmanblog.org/proto-orthodox-writers/',
            'https://ehrmanblog.org/heresy-and-orthodoxy/',
            'https://ehrmanblog.org/constantine/',
            'https://ehrmanblog.org/jesus-and-film/',
            'https://ehrmanblog.org/teaching-christianity/',
            'https://ehrmanblog.org/reflections-ruminations/',
            'https://ehrmanblog.org/public-forum/',
        ];
        $this->articlePageLinks = [];
    }

    public function scrape(): void
    {
        foreach ($this->categoriesToScrape as $categoryToScrap) {
            $this->collectArticlePageLinksForCategory($categoryToScrap);
            $this->scrapeCollectedArticlePages();
        }
    }

    private function collectArticlePageLinksForCategory(string $categoryToScrap): void
    {
        $response = $this->client->post($categoryToScrap);
        $crawler = new Crawler((string)$response->getBody());

        do {
            $articlePageLinks = $this->scrapeCategoryPage($crawler);
            $this->articlePageLinks = array_merge($this->articlePageLinks, $articlePageLinks);
        } while ($crawler = $this->turnCategoryPage($crawler));
    }

    private function scrapeCategoryPage(Crawler $crawler): array
    {
        $articlePageLinks = $crawler->filter(
            'body > #boxed-wrapper > #wrapper > #main > .fusion-row > #content > #posts-container > .fusion-posts-container > article'
        )->each(
            function (Crawler $node) {
                try {
                    $readMoreButton = $node->filter('div > div > .fusion-read-more')->each(
                        function (Crawler $node) {
                            try {
                                $readMoreButton = $node->getNode(0);
                                $articlePageLink = $readMoreButton->getAttribute('href');
                            } catch (\Exception $ex) {
                                $articlePageLink = '';
                            }

                            return $articlePageLink;
                        }
                    );

                    $articlePageLink = $readMoreButton[0];
                } catch (\Exception $ex) {
                    $articlePageLink = '';
                }

                return $articlePageLink;
            }
        );

        return array_filter($articlePageLinks);
    }

    private function turnCategoryPage(Crawler $crawler): ?Crawler
    {
        $nextPageLink = $crawler->filter(
            'body > #boxed-wrapper > #wrapper > #main > .fusion-row > #content > #posts-container > .pagination > .pagination-next'
        )->each(
            function (Crawler $node) {
                try {
                    $nextPageButton = $node->getNode(0);
                    $nextPageLink = $nextPageButton->getAttribute('href');
                } catch (\Exception $ex) {
                    $nextPageLink = '';
                }

                return $nextPageLink;
            }
        );

        if (!isset($nextPageLink[0])) {
            return null;
        }

        $nextPageLink = $nextPageLink[0];
        $response = $this->client->post($nextPageLink);

        return new Crawler((string)$response->getBody());
    }

    private function scrapeCollectedArticlePages(): void
    {
        while($articlePageLink = array_shift($this->articlePageLinks)) {
            $this->scrapeArticlePage($articlePageLink);
        }
    }

    private function scrapeArticlePage(string $articlePageLink): void
    {
        $response = $this->client->post($articlePageLink, ['cookies' => $this->config]);
        $crawler = new Crawler((string)$response->getBody());

        $articleData = $crawler->filter(
            'body > #boxed-wrapper > #wrapper > #main > .fusion-row > #content > article'
        )->each(
            function (Crawler $node) {
                try {
                    $articleTitle = $node->filter('h1')->text();
                    $articleContent = $node->filter('.post-content')->text();
                    $articleData = ['title' => $articleTitle, 'content' => $articleContent];
                } catch (\Exception $ex) {
                    $articleData = ['title' => '', 'content' => ''];
                }

                return $articleData;
            }
        );

        if (!isset($articleData[0])) {
            return;
        }

        $articleData = $articleData[0];
        $this->saveArticle($articleData);
    }

    private function saveArticle(array $articleData): void
    {
        $articleData['title'] = str_replace(' ', '_', $articleData['title']);
        $articleData['title'] = str_replace('/', '_', $articleData['title']);
        $articleFilePath = sprintf(self::ARTICLE_PATH, $articleData['title']);
        if (file_exists($articleFilePath)) {
            return;
        }

        file_put_contents($articleFilePath, $articleData['content']);
    }
}

$bartEhrmanBlogScraper = new BartEhrmanBlogScraper();
$bartEhrmanBlogScraper->scrape();