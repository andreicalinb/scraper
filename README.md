# What

This is a simple scraper made by myself.

# Why

The scope was to scrape all articles from [this](https://ehrmanblog.org/) blog.

# How to use

1. Execute `compose install`
2. Login on the blog, take your login cookie and replace it into the code.
3. Optional: change forum categories that you want to be scraped.
4. Execute `php index.php`

Note: Blog DOM might change in time so the CSS selectors might also need to be changed.